# 路云链路
智路OS包含云端接入服务，开发者可以通过自定义配置接入支持标准MQTT Client SDK的云平台。同时云端接入服务支持扩展HTTP、WebSocket等协议，支持数据的上行和下行。   
智路OS项目提供了[智路OS开发者平台](https://airos.baidu.com)供开发者体验路云链路，自定义配置完成并启动开源项目后，可在平台观测到相关数据。

## 1.快速开始
编译完成获得产出后，可以使用`road_message_to_cloud.launch`拉起信号灯的整体处理流程。
执行以下命令：

```bash
source /airos/setup.bash # 初始化运行环境
nohup cyber_launch start /airos/air_service/launch/road_message_to_cloud.launch & # 拉起处理流程
```

查看云端上传数据：
```bash
cyber_channel echo /v2x/cloud/report/mqtt
```

## 2.模块介绍
### 2.1 V2X报文上传
`message_reporter`报文上传模块负责将V2X编码后的数据上传到云端
### 2.2 RSI、MAP生成
`rsi_generator`模块在获取到感知数据后，提取其中的事件信息，生成RSI
`v2x_codec`模块在读取本地`rsu_map.xml`数据后，转换成V2X标准MAP格式
### 2.3 感知数据上传
`perception_adapter`感知适配器负责将感知数据转换成云端格式和V2X编码需要的RSM、SSM等数据格式
### 2.4 基础监控数据
`monitor`基础监控模块负责监控系统各模块，包括设备状态、数据质量等
### 2.5 云端接入服务
`cloud_service`云端接入服务提供了标准的MQTT客户端接入实现，可接入任何支持开源MQTT Client SDK的云端平台。  
当前应用支持上传数据到`智路OS开发者平台`，相关topic如下：
| 数据 | Topic |
| :----: | :----: |
| 信号灯pb数据 | upload/trafficlight/SN |
| 感知pb数据 | upload/event/SN |
| 设备监控数据 | upload/status/SN |
| 报文数据 | upload/v2i/xxx/SN |

当前云端接入模块`cloud_service`随信号灯`traffic_light_pipeline.launch`一同被拉起，如不需要信号灯相关服务，可以单独拉起云端服务：
```bash
source /airos/setup.bash # 初始化运行环境
nohup mainboard -d /airos/middleware/device_service/cloud/dag/cloud_service.dag -p cloud_service & # 拉起云端服务
```

## 3.云端服务自定义配置
### 3.1 连接信息配置
如需使用MQTT接入真实的云端平台，可修改MQTT连接配置：
`/airos/middleware/device_service/cloud/conf/standard_mqtt.yaml`
```yaml
mqtt:
  username: v2x_client
  password: password
  cloud_url: tcp://127.0.0.1:1883
  client_id: $sn
  subscribe_topic:
    - v2x/event/$sn
    - v2x/$sn/map
```
其中`client_id`和`subscribe_topic`中的通配符`$sn`会在程序启动时会替换为本机SN。  
本机SN文件路径为`/etc/v2x/SN`，开发者可修改设备SN：
```bash
echo NEW_RSCU_SN | sudo tee /etc/v2x/SN
```
### 3.2 智路OS开发者平台
智路OS项目提供了[智路OS开发者平台](https://airos.baidu.com)供开发者体验路云链路，连接方式如下：  
- 1.注册智路OS开发者平台。   
- 2.在开发者平台注册RSCU设备，获取到设备SN后修改默认的SN文件。   
- 3.获取连接到平台的URL、用户名、密码，分别填入云端接入连接信息配置所述配置文件中的cloud_url、username和password。   
- 4.启动相应的信号灯或者感知pipeline，即可在开发者平台查看相应的数据结果。

更详细的开发者平台使用方式，请参考开发者平台提供的使用手册。