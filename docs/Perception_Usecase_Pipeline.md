# Perception Usecase Pipeline


## 1.事件检测框图
![多传感器障碍物融合框图](./images/perception_usecase_pipeline.png)

## 2.快速开始

事件检测进程依赖于多传感器融合进程的输出，### TODO 拉起前驱进程

事件检测进程拉起：####TODO
```
cyber_launch start air_service/launch/usecase.launch
```
事件检测可视化进程拉起：
```
cyber_launch start air_service/launch/usecase_visualization.launch
```
## 3.模块介绍
### 3.1 目录结构
```
perception-usecase
├── conf
├── dag
├── pipeline
├── proto
├── usecase
│   ├── algorithm
│   ├── base
│   ├── common
│   └── map
└── visualization
```
- perception-usecase/conf: 包括事件检测的配置以及具体路口的配置
- perception-usecase/dag: 通过cyberrt中间件拉起事件检测进程的启动脚本
- perception-usecase/pipeline: 事件检测运行Pipeline
- perception-usecase/proto: 事件检测中使用的字段
- perception-usecase/usecase: 事件检测代码构成
    + algorithm: 事件算法类
    + base: 组件类
    + common:基础类
    + map: 地图服务类
- perception-usecase/visualization: 可视化组件

### 3.2 事件检测架构设计


### 3.3 事件检测可视化

通过Opencv的GUI功能将障碍物的位置和尺寸等信息显示到屏幕中。此外，可视化程序支持通过手动调节滑动块的阈值以达到屏幕输出不同信息。

**使用方法：**
- 按钮 lane 
    * lane = 0 : 不显示道路信息
    * lane = 1 : 显示道路边界
    * lane = 2 : 显示道路方向，绿色圈为道路终点
    * lane = 3 : 同时显示道路边界和道路朝向
- 按钮 lane_congestion
    * lane_congestion = 0 : 不显示车道拥堵相关信息
    * lane_congestion = 1 : 显示车道拥堵相关信息，包括停止点，越线区域，障碍物停止时间
- 按钮 perception_id
    * perception_id = 0 : 不显示障碍物 perception_id
    * perception_id = 1 : 显示障碍物 perception_id
- 按钮 perception_type
    * perception_type = 0 : 不显示障碍物 perception_type
    * perception_type = 1 : 显示障碍物 perception_type
- 按钮 high_light_obj
    * high_light_obj = 1 :
        高亮显示障碍物，当调整按钮为1时，跳出新窗口监听键盘输入，需要使用主键盘区域数字键，输入障碍物perception_id，回车键入，可实现目标障碍物高亮。
- 按钮 draw_polygon
    + draw_polygon = 0 : 不进行绘图
    + draw_polygon = 1 : 绘制区域Polygon
        * <kbd>ctrl</kbd>+<kbd>鼠标左键</kbd> \
            增加点，且只会在当前Polygon中增加点。
        * <kbd>ctrl</kbd>+<kbd>鼠标右键</kbd> \
            删除点，若当前Polygon的点删除完毕，并且不是末端空点集，会继续向上删除另一个Polygon的点，直到全部点删除完毕。
        * <kbd>shift</kbd>+<kbd>鼠标左键</kbd> \
            切换Polygon
        * <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>鼠标左键</kbd> \
            增加Polygon，完成某个Polygon绘制后，可以增加Polygon继续绘制。
        * <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>鼠标右键</kbd> \
            将绘制完成的Polygon用Proto格式保存到指定文件夹 \
            (默认路径是/airos/output/modules/perception-usecase/conf/target.pt)
    + draw_polygon = 2 : 绘制车道Polygon，车道polygon由车道边界点和车道中心点组成。
        * <kbd>ctrl</kbd>+<kbd>鼠标左键</kbd> \
            增加车道边界点
        * <kbd>ctrl</kbd>+<kbd>鼠标右键</kbd> \
            删除车道边界点
        * <kbd>shift</kbd>+<kbd>鼠标左键</kbd> \
            增加车道方向点
        * <kbd>shift</kbd>+<kbd>鼠标右键</kbd> \
            删除车道方向点
        * <kbd>ctrl</kbd>+<kbd>鼠标中键</kbd> \
            切换车道
        * <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>鼠标左键</kbd> \
            增加车道Polygon, 某个车道画完毕之后，可以增加车道继续绘制
        * <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>鼠标右键</kbd> \
            保存绘制完成的车道Polygon到指定文件夹 \
            (默认路径是/airos/output/modules/perception-usecase/conf/lane_param.pt)



>如果您需要使用智路OS开发者平台，或者在开发使用过程中遇到任何问题，欢迎通过以下方式联系我们：  
> Email：zhiluos@163.com