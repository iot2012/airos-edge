/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/**
 * @file     device_base.h
 * @brief    ins设备接口
 * @version  V1.0.0
 */

#pragma once

#include <functional>
#include <memory>

#include "base/device_connect/proto/ins_data.pb.h"

namespace os {
namespace v2x {
namespace device {

/**
 * @struct  InsDeviceState
 * @brief   Ins设备运行状态
 * @details
 */
enum class InsDeviceState { UNKNOWN, NORMAL, OFFLINE };

using GnssPBDataTypePtr = std::shared_ptr<const AoGPSFrame>;
using GnssCallBack = std::function<void(const GnssPBDataTypePtr&)>;
using IMUPBDataTypePtr = std::shared_ptr<const AoIMUFrame>;
using IMUCallBack = std::function<void(const IMUPBDataTypePtr&)>;
/**
 * @brief  Ins设备接口类
 */
class InsDevice {
 public:
  InsDevice() = default;
  /**
   * @brief      Ins设备构造函数
   * @param[in]  cb_1 AirOS-edge框架注册的回调函数
   * @param[in]  cb_2 AirOS-edge框架注册的回调函数
   */
  InsDevice(const GnssCallBack& cb_1, const IMUCallBack& cb_2)
      : sender_1_(cb_1), sender_2_(cb_2) {}
  virtual ~InsDevice() = default;
  /**
   * @brief      用于Ins设备初始化
   * @param[in]  config_file Ins设备初始化参数配置文件
   * @retval     初始化是否成功
   */
  virtual bool Init(const std::string& config_file) = 0;
  /**
   * @brief      用于启动Ins设备，产出AirOS-edge结构化的标准Ins输出数据
   */
  virtual void Start() = 0;
  /**
   * @brief      用于获取ins设备状态
   * @retval     设备状态码InsDeviceState
   */
  virtual InsDeviceState GetState() = 0;

 protected:
  GnssCallBack sender_1_;
  IMUCallBack sender_2_;
};

}  // namespace device
}  // namespace v2x
}  // namespace os
