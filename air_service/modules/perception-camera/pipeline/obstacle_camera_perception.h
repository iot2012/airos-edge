/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "base/plugin/algorithm_plugin.h"
#include "air_service/modules/perception-camera/algorithm/detector/detector_plugin_adapter.h"
#include "air_service/modules/perception-camera/algorithm/interface/perception_frame.h"
#include "air_service/modules/perception-camera/algorithm/roi_filter/roi_judgement_adapter.h"
#include "air_service/modules/perception-camera/algorithm/tracker/tracker_plugin_adapter.h"
#include "air_service/modules/perception-camera/algorithm/transformer/transformer_plugin_adapter.h"
#include "air_service/modules/perception-camera/proto/perception_camera.pb.h"

namespace airos {
namespace perception {
namespace camera {

struct ObstacleCameraPerceptionParam {
  std::vector<airos::base::PluginParam> plugin_params;
};

class ObstacleCameraPerception {
 public:
  ObstacleCameraPerception() = default;
  ObstacleCameraPerception(const ObstacleCameraPerception &) = delete;
  ObstacleCameraPerception &operator=(const ObstacleCameraPerception &) =
      delete;
  ~ObstacleCameraPerception() = default;
  bool Init(const ObstacleCameraPerceptionParam &param) {
    // std::vector<airos::base::PluginParam> plugin_param;//=
    // {"DetectorPluginAdapter","RoiPluginAdapter","TrackerPluginAdapter","TransformerPluginAdapter"};
    return plugin_manager_.Init(param.plugin_params);
  }
  bool Perception(PerceptionFrame &frame) { return plugin_manager_.Run(frame); }
  std::string Name() const { return "ObstacleCameraPerception"; }

 private:
  airos::base::PluginManager<PerceptionFrame> plugin_manager_;
};

REGISTER_ALGORITHM_PLUGIN_ELEMENT(DetectorPluginAdapter, PerceptionFrame);
REGISTER_ALGORITHM_PLUGIN_ELEMENT(RoiPluginAdapter, PerceptionFrame);
REGISTER_ALGORITHM_PLUGIN_ELEMENT(TrackerPluginAdapter, PerceptionFrame);
REGISTER_ALGORITHM_PLUGIN_ELEMENT(TransformerPluginAdapter, PerceptionFrame);

}  // namespace camera
}  // namespace perception
}  // namespace airos
