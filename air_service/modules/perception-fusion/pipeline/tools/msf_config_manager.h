#pragma once

#include <map>

#include "base/common/singleton.h"
#include "boost/function.hpp"
#include "boost/geometry.hpp"
#include "boost/geometry/geometries/geometries.hpp"
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "air_service/modules/perception-fusion/pipeline/tools/msf_gflags.h"
#include "air_service/modules/perception-fusion/proto/fusion_params.pb.h"
#include "air_service/modules/perception-fusion/proto/multi_sensor_fusion_config.pb.h"
// #include "air_service/modules/perception-fusion/proto/track_params.pb.h"

namespace airos {
namespace perception {
namespace msf {
namespace tools {

class MsfConfigManager {
 public:
  MsfConfigManager();
  const airos::perception::fusion::MsfConfig& GetMsfConfig() const {
    return msf_config_;
  }
  const airos::perception::fusion::FusionParams& GetFusionParams() const {
    return fusion_params_;
  }

 private:
  bool LoadMsfConfig(const std::string&);
  bool LoadFusionParams(const std::string&);

 private:
  airos::perception::fusion::MsfConfig msf_config_;
  airos::perception::fusion::FusionParams fusion_params_;
};

}  // namespace tools
}  // namespace msf
}  // namespace perception
}  // namespace airos