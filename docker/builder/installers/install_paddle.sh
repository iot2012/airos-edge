#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_paddle {
    local url="https://zhilu.bj.bcebos.com/paddle.tar.gz"
    local sha256="27afcd3d9d282485f4c1df2e8b9e7bc70c359ef76e8234d5c6638d5fd19c92cc"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "paddle.tar.gz" "${sha256}"; then
            tar -zxf paddle.tar.gz
        else
            echo "check_sha256 failed: paddle.tar.gz"
        fi
        rm paddle.tar.gz
    popd >/dev/null
}

function main {
    install_paddle
}

main "$@"
