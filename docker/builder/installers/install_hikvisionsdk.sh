#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_hikvisionsdk {
    local url="https://zhilu.bj.bcebos.com/hikvision-sdk.tar.gz"
    local sha256="f612ce45ab6e52a8f1c3d4f0e0efa21fb5ca21630bd79c5598b0cf4ea39604dc"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "hikvision-sdk.tar.gz" "${sha256}"; then
            tar -zxf hikvision-sdk.tar.gz
        else
            echo "check_sha256 failed: hikvision-sdk.tar.gz"
        fi
        rm hikvision-sdk.tar.gz
    popd >/dev/null
}

function main {
    install_hikvisionsdk
}

main "$@"