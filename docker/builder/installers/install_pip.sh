#!/usr/bin/env bash

set -e

function install_pip {
    pushd /opt/ >/dev/null
        curl https://bootstrap.pypa.io/pip/3.6/get-pip.py -o get-pip.py && \
            python3 get-pip.py --force-reinstall && rm -f /opt/get-pip.py
    popd >/dev/null
}

function main {
    [ -x "$(command -v python3)" ] && install_pip
}

main "$@"
